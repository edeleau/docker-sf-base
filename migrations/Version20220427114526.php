<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Psr\Log\LoggerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220427114526 extends AbstractMigration
{


    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $fromName = $_ENV['MAILER_FROMNAME'];
        $fromEmail = $_ENV['MAILER_FROMMAIL'];
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("INSERT INTO `mail__template` (`id`, `slug`, `subject`, `twig`, `from_name`, `from_email`, `copy_admin`) VALUES (NULL, 'new_account', 'Création de votre compte', 'new_account.html.twig', '" . $fromName . "',  '" . $fromEmail . "', '0');");
        $this->addSql("INSERT INTO `mail__template` (`id`, `slug`, `subject`, `twig`, `from_name`, `from_email`, `copy_admin`) VALUES (NULL, 'confirm_account', 'Vérification de votre compte', 'confirmation_email.html.twig', '" . $fromName . "', '" . $fromEmail . "', '0');");
        $this->addSql("INSERT INTO `mail__template` (`id`, `slug`, `subject`, `twig`, `from_name`, `from_email`, `copy_admin`) VALUES (NULL, 'reset_password', 'Réinitialisation de votre mot de passe', 'reset_password_email.html.twig', '" . $fromName . "', '" . $fromEmail . "', '0');");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
