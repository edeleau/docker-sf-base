<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220422082009 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE mail__notification (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, recipient VARCHAR(255) NOT NULL, flag VARCHAR(255) NOT NULL, type VARCHAR(30) NOT NULL, error_code VARCHAR(30) DEFAULT NULL, status VARCHAR(50) DEFAULT NULL, content LONGTEXT DEFAULT NULL, additional_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mail__template (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(255) NOT NULL, subject VARCHAR(255) NOT NULL, twig VARCHAR(255) NOT NULL, from_name VARCHAR(255) NOT NULL, from_email VARCHAR(255) NOT NULL, copy_admin TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE mail__notification');
        $this->addSql('DROP TABLE mail__template');
    }
}
