DOCKER = docker compose exec
BACKEND = php
FRONT = node
DB = database
DB_NAME = db_name

install:
	$(DOCKER) $(BACKEND) composer install
update:
	$(DOCKER) $(BACKEND) composer update
%:
	$(DOCKER) $(BACKEND) php bin/console make:$@
migrate:
	$(DOCKER) $(BACKEND) php bin/console doctrine:migrations:migrate
generate:
	$(DOCKER) $(BACKEND) php bin/console doctrine:migrations:generate
watch:
	$(DOCKER) $(FRONT) yarn encore dev --watch
up:
	$(DOCKER) compose up -d
cc:
	$(DOCKER) $(BACKEND) php bin/console c:c 
init:
	docker compose run --rm --no-deps $(FRONT) bash -ci 'yarn install'
	docker compose up -d
	make update	
	$(DOCKER) $(BACKEND) php bin/console doctrine:database:create
	make migrate
	make add-user
cs-fixer:
	$(DOCKER) $(BACKEND) tools/php-cs-fixer/vendor/bin/php-cs-fixer fix src --config php-cs-fixer.dist.php
# user  test@test.fr avec pour mdp admin
add-user:
	$(DOCKER) $(DB) mysql -uroot -proot $(DB_NAME) -e "INSERT INTO user( roles ,email, password,is_verified) VALUES ('[\"ROLE_ADMIN\"]','test@test.fr','\$$argon2id\$$v=19\$$m=65536,t=4,p=1\$$ZmFXMHRlQWNvMGJjZTRVVQ\$$Xe6fO15+cU4zaqrzQdnndA', 1)";

	