<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class UserVoter extends Voter
{
    public const EDIT = 'POST_EDIT';
    public const VIEW = 'POST_VIEW';

    protected function supports(string $attribute, $subject): bool
    {
        if (
            \in_array($attribute, [self::EDIT, self::VIEW], true)
            && $subject instanceof \App\Entity\User
        ) {
            return true;
        }

        return false;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $currentUser = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$currentUser instanceof UserInterface) {
            return false;
        }

        $user = $subject;
        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($user, $currentUser);
                break;
            case self::VIEW:
                return $this->canView($user, $currentUser);
                break;
        }

        return false;
    }

    private function canView(UserInterface $user, UserInterface $currentUser)
    {
        return $user === $currentUser;
    }

    private function canEdit(UserInterface $user, UserInterface $currentUser)
    {
        return $user === $currentUser;
    }
}
