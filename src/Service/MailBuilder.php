<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class MailBuilder
{
    protected $adminMail;
    protected $toName;
    protected $mailer;

    protected LoggerInterface $logger;
    protected EntityManagerInterface $em;

    public const PATHTEMPLATE = 'mail/';

    public function __construct(
        string $adminMail,
        MailerInterface $mailer,
        LoggerInterface $logger,
        EntityManagerInterface $em
    ) {
        $this->adminMail = $adminMail;
        $this->mailer = $mailer;
        $this->logger = $logger;
        $this->em = $em;
    }

    public function createMail(
        string $fromEmail,
        string $fromName,
        string $toEmail,
        string $toName,
        string $subject,
        string $htmlTemplate,
        array $files = [],
        array $data = [],
        $copyAdmin = false
    ): TemplatedEmail {
        $email = (new TemplatedEmail())
            ->from(new Address($fromEmail, $fromName))
            ->to(new Address($toEmail, $toName))
            ->subject($subject)
            ->htmlTemplate(self::PATHTEMPLATE . $htmlTemplate)
            ->context($data);
        if ($copyAdmin) {
            $email->bcc($this->adminMail);
        }
        foreach ($files as $file) {
            $email->attachFromPath($file->getRealPath(), $file->getClientOriginalName(), $file->getClientMimeType());
        }

        return $email;
    }

    public function sendMail(TemplatedEmail $email): TemplatedEmail
    {
        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->logger->log(Logger::CRITICAL, $e . "\r\n");

            return ['error' => 'Problème d\'envoi, veuillez réessayer plus tard'];
        }

        return $email;
    }
}
