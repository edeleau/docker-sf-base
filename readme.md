## Container docker Symfony 6 + PHP8 

# Présentation du projet : 
Le projet a pour but d'être un container plug and play. Juste à cloner, intialiser et vous pouvez bosser !

# Liste des étapes pour lancer le container : 
- changer les variables :
    -   Dans le .env : 
    ```
    changer le DATABASE_URL et les variables de MAILER dont vous avez besoin
    ```
    -   Dans le makefile : 
    ```
    changer le DB_NAME
    ```
- Initialiser les containers :
```
make init
```
- Le projet est fonctionnel avec un utilisateur test@test.fr qui a comme mdp admin !

# Les commandes existantes pour vous faciliter la tâche: 
- Créer un(e) controller/entité/migration  : 
``` 
make controller
make entity
make migration
```
- Lancer les containers : 
```
make up
```
- Lancer les migration : 
```
make migrate
```
